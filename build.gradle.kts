allprojects {
    group = "xyz.jbneu.sandbox"
    version = "1.0-SNAPSHOT"
}

subprojects {
    repositories {
        jcenter()
        mavenCentral()
    }
}
