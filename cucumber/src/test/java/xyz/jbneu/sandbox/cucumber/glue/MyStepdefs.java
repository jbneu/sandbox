package xyz.jbneu.sandbox.cucumber.glue;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class MyStepdefs {

    @Given("blah")
    public void blah() {
        System.out.println("blah");
    }

    @When("do")
    public void _do() {
        System.out.println("do");
    }


    @Then("should be blah")
    public void should_be_blah() {
        System.out.println("should be blah");
    }

}
