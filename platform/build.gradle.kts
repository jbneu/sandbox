plugins {
    `java-platform`
}

dependencies {
    constraints {
        api("io.cucumber:cucumber-java:5.6.0")
        api("io.cucumber:cucumber-junit-platform-engine:5.6.0")

        api("org.junit.jupiter:junit-jupiter-api:5.6.1")
        runtime("org.junit.jupiter:junit-jupiter-engine:5.6.1")
    }
}
